site.yml (file)

---
- hosts: myservers
  become: yes
  become_user: root
  vars:
    ansible_become_password: "ваш_пароль_sudo"
  roles:
    - role: geerlingguy.postgresql
      postgresql_version: 16
      postgresql_repos:
        - repo: deb http://apt.postgresql.org/pub/repos/apt/ jammy-pgdg main
          state: present
      postgresql_packages:
        - postgresql-16


ini.yml (file)

[myservers]
192.168.19.129 ansible_user=db
